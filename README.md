# Samply Store REST

Samply Store REST provides a RESTful interface to Samply Store.  The inteface
uses the OSSE XML schemas defined [here](http://schema.samply.de/osse/).

This application does not use authentication and therefore should *not* be publicly
accessible.

# Features

- get metadata about the registry
- get all medical data
- asynchronous query execution


# Build

In order to build this project, you need to configure maven properly.  See
[Samply.Maven](https://bitbucket.org/medinfo_mainz/samply.maven) for more
information.

Use maven to build the `war` file:

```
mvn clean package
```
