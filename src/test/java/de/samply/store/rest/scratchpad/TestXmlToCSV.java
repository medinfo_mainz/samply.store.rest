/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest.scratchpad;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import de.samply.share.model.osse.QueryResult;
import de.samply.share.utils.XmlToCSV;


/**
 * This class converts a QueryResult into a CSV, with attached mappings.
 * This code has been copied to the Samply.Share project.
 *
 *
 */
public class TestXmlToCSV {

    public static void main(String[] args) throws JAXBException, IOException {
        File file = new File("test.xml");

        JAXBContext context = JAXBContext.newInstance(QueryResult.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        QueryResult result = (QueryResult) unmarshaller.unmarshal(new FileReader(file));

        XmlToCSV csv = new XmlToCSV();

        StringWriter csvWriter = new StringWriter();
        StringWriter mappingWriter = new StringWriter();

        csv.convert(result, csvWriter, mappingWriter);

        System.out.println("CSV: \n" + csvWriter.toString() + "\n\n");

        System.out.println("Mapping: \n" + mappingWriter.toString());
    }

}
