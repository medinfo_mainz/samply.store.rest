/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest.scratchpad;
import java.sql.SQLException;
import java.util.Date;
import java.util.Random;

import javax.xml.bind.JAXBException;

import de.samply.store.AccessController;
import de.samply.store.PSQLModel;
import de.samply.store.Resource;
import de.samply.store.TimestampLiteral;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEModel;
import de.samply.store.osse.OSSEVocabulary;

@Deprecated
public class PatientCreator {

    private static final String blutdruck = "urn:osse-mdr1:dataelement:34:3";
    private static final String geschlecht = "urn:osse-mdr1:dataelement:33:2";
    private static final String blutzucker = "urn:osse-mdr1:dataelement:19:34";

    private static Random rand = new Random();

    public static void main(String[] data) throws DatabaseException, JAXBException, SQLException {
        OSSEModel.init();

        PSQLModel<Void> model = new PSQLModel<Void>(new AccessController<Void>(),
                "localhost", "osse", "paul", "");

        model.injectLogin("export");

        model.beginTransaction();

        Resource location = model.getResources(OSSEVocabulary.Type.Location).get(0);

        Resource status = model.createResource(OSSEVocabulary.Type.Status);
        status.setProperty("name", "open");
        model.saveOrUpdateResource(status);

        String[] sexes = new String[] {"m", "f", "u"};

        for(int i = 0; i < 100; ++i) {
            System.out.println("Creating patient " + i);

            Resource patient = model.createResource(OSSEVocabulary.Type.Patient);
            patient.setProperty("pseudonym", "PSN"+ rand.nextInt(1000000));

            String gender = sexes[rand.nextInt(3)];

            model.saveOrUpdateResource(patient);

            int caseNum = rand.nextInt(2) + 1;
            for(int ci = 0; ci < caseNum; ++ci) {
                Resource c = model.createResource(OSSEVocabulary.Type.Case);
                c.setProperty(OSSEVocabulary.Case.Patient, patient);
                c.setProperty(OSSEVocabulary.Case.Location, location);

                c.setProperty("lastUpdated", new TimestampLiteral(new Date().getTime()));

                model.saveOrUpdateResource(c);

                Resource caseForm = model.createResource(OSSEVocabulary.Type.CaseForm);
                caseForm.setProperty(geschlecht, gender);
                caseForm.setProperty(OSSEVocabulary.CaseForm.Case, c);
                caseForm.setProperty(OSSEVocabulary.CaseForm.Name, "gender-form");
                caseForm.setProperty(OSSEVocabulary.CaseForm.Status, status);
                caseForm.setProperty(OSSEVocabulary.CaseForm.Version, "1");

                model.saveOrUpdateResource(caseForm);

                int epNum = rand.nextInt(4) + 1;

                for(int ei = 0; ei < epNum; ++ei) {
                    Resource episode = model.createResource(OSSEVocabulary.Type.Episode);

                    episode.setProperty(OSSEVocabulary.Episode.Case, c);
                    episode.setProperty(OSSEVocabulary.Episode.Name, "test-episode");

                    model.saveOrUpdateResource(episode);

                    int epFormNum = rand.nextInt(5) + 1;

                    for(int fi = 0; fi < epFormNum; ++fi) {
                        Resource epiForm = model.createResource(OSSEVocabulary.Type.EpisodeForm);
                        epiForm.setProperty(blutdruck, rand.nextInt(40) + 80);
                        epiForm.setProperty(blutzucker, rand.nextInt(90) + 80);
                        epiForm.setProperty(OSSEVocabulary.EpisodeForm.Episode, episode);
                        epiForm.setProperty(OSSEVocabulary.EpisodeForm.Name, "blood-form");
                        epiForm.setProperty(OSSEVocabulary.EpisodeForm.Status, status);
                        epiForm.setProperty(OSSEVocabulary.EpisodeForm.Version, "1");

                        model.saveOrUpdateResource(epiForm);
                    }
                }
            }
        }

        model.commit();

        model.close();
    }

}
