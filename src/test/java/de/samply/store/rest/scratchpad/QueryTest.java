/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
/**
 *
 */
package de.samply.store.rest.scratchpad;

import de.samply.store.DatabaseDefinitions;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEModel;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.ResourceQuery;
import de.samply.store.sql.SQLColumn;
import de.samply.store.sql.SQLColumnType;
import de.samply.store.sql.SQLQuery;
import de.samply.store.sql.SQLStringValue;
import de.samply.store.sql.SQLTemporaryTable;
import de.samply.store.sql.clauses.EqualClause;

/**
 *
 */
public class QueryTest {

    public static void main(String[] args) throws DatabaseException {
        OSSEModel.init();

        ResourceQuery res = new ResourceQuery(OSSEVocabulary.Type.Episode);

        SQLQuery superQuery = res.prepareSQLQuery();

        SQLQuery query = new SQLQuery(DatabaseDefinitions.get(OSSEVocabulary.Type.EpisodeForm).table);
        query.addSelection(query.getMainTable().getColumn("episode_id", SQLColumnType.INTEGER));
        SQLColumn jsonArrayColumn = query.getMainTable().getJsonArrayColumn("data", "urn:osse-8:record:1:1,column");
        query.addSelection(jsonArrayColumn);
        query.addClause(new EqualClause(query.getMainTable().getColumn("name", SQLColumnType.STRING), new SQLStringValue("form_91_ver-1")));

//        System.out.println(query.construct());

        SQLTemporaryTable t = new SQLTemporaryTable(query, superQuery);
//
        superQuery.addJoin(t, t.getOutsideColumn(query.getMainTable().getColumn("episode_id")),
                superQuery.getMainTable().getColumn("id", SQLColumnType.INTEGER));

        superQuery.addClause(new EqualClause(t.getOutsideColumn(jsonArrayColumn, SQLColumnType.JSON_STRING, "urn:osse-8:dataelement:6:1"),
                new SQLStringValue("2011")));

        System.out.println(superQuery.construct());
    }

}
