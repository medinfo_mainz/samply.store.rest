/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest;

import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.samply.share.model.osse.And;
import de.samply.share.model.osse.Attribute;
import de.samply.share.model.osse.Error;
import de.samply.share.model.osse.Gt;
import de.samply.share.model.osse.Leq;
import de.samply.share.model.osse.ObjectFactory;
import de.samply.share.model.osse.QueryResult;
import de.samply.share.model.osse.RorMetareg;
import de.samply.share.model.osse.View;
import de.samply.share.model.osse.Where;
import de.samply.store.PSQLModel;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.osse.exception.UnknownAttributesException;
import de.samply.store.osse.query.QueryConfig;
import de.samply.store.osse.query.QueryUtil;
import de.samply.store.osse.query.ResourceQueryBuilder;
import de.samply.store.query.ResourceQuery;
import de.samply.store.rest.dao.ConnectionFactory;
import de.samply.store.rest.dao.DAOException;
import de.samply.store.rest.dao.Request;
import de.samply.store.rest.dao.RequestDAO;
import de.samply.store.rest.dao.RequestStatus;
import de.samply.store.rest.dao.StoreConnection;

/**
 * The thread that executes the query and stores the result in the database.
 *
 */
public class QueryThread extends Thread {

    private static final Logger logger = LogManager.getLogger(QueryThread.class);

    public enum FormType {CASE, EPISODE};

    private ObjectFactory factory = new ObjectFactory();

    /**
     * The set of ignored URNs for Metareg (Case count and date of case count).
     */
    private static final HashSet<String> ignoredURNsMetareg = new HashSet<>(
            Arrays.asList("urn:osse-ror:dataelement:21:1", "urn:osse-ror:dataelement:22:1"));


    /**
     * The query ID that this QueryThread executes
     */
    private final int queryId;

    /**
     * The where part of the view.
     */
    private Where where = null;

    /**
     * Creates a new QueryThread for the specified query ID.
     * @param queryId
     */
    public QueryThread(int queryId) {
        this.queryId = queryId;
    }

    /**
     * Loads the query from the database. Checks if the query is a query for the
     * registry of registries. If it is, update the dynamic fields (case count and case count date)
     * and save the result in the database.
     * If it is not a
     */
    @Override
    public void run() {
        logger.debug("Query Thread started for query id " + queryId);

        PSQLModel<Void> model = null;

        try(StoreConnection store = ConnectionFactory.get()) {
            model = ConnectionFactory.getModel();
            model.injectLogin("export");

            RequestDAO dao = store.get(RequestDAO.class);
            Request request = dao.getRequest(queryId);

            JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);
            View view = (View) context.createUnmarshaller().unmarshal(new StringReader(request.getQuery()));

            if(view.getQuery() != null) {
                where = view.getQuery().getWhere();

                if(where == null) {
                    where = new Where();
                }
            }

            List<String> keys = Collections.emptyList();

            if(view.getViewFields() != null) {
                keys = view.getViewFields().getMdrKey();
            }

            ArrayList<Resource> registries = model.getResources(new ResourceQuery(OSSEVocabulary.Type.Registry));

            Object target = null;

            if(registries.size() > 0 && keys.size() > 0)  {
                logger.debug("Found registry object");
                logger.debug("Checking if the viewfields are known to this registry object");

                boolean validMetaReg = true;

                Resource registry = registries.get(0);

                /**
                 * Check if the query is a "metareg" query. If
                 * there is one key, that is not in the registry object and
                 * is not ignored by the interface, the query is not a valid metareg query.
                 */

                for(String key : keys) {
                    if(!registry.getDefinedProperties().contains(key) &&
                            ! ignoredURNsMetareg.contains(key)) {
                        validMetaReg = false;
                    }
                }

                if(validMetaReg) {
                    QueryResult result = new QueryResult();
                    result.setId("" + queryId);
                    SimpleDateFormat format = new SimpleDateFormat("YYYY-dd-MM");

                    logger.debug("All viewfields are known to this registry object. Using RorMetareg.");
                    RorMetareg reg = new RorMetareg();

                    int cases = model.getResources(OSSEVocabulary.Type.Case).size();
                    registry.setProperty("urn:osse-ror:dataelement:21:1", cases);
                    registry.setProperty("urn:osse-ror:dataelement:22:1", format.format(new Date()));

                    for(String key : keys) {
                        Attribute attr = new Attribute();
                        attr.setMdrKey(key);
                        attr.setValue(factory.createValue(registry.getProperty(key).getValue()));
                        reg.getAttribute().add(attr);
                    }

                    result.getEntity().add(reg);

                    target = result;
                } else {
                    /**
                     * So this is not a metareg query... execute as a patient query.
                     */
                    target = executePatientQuery(view, model);
                }
            } else {
                /**
                 * So this is not a metareg query... execute as a patient query.
                 */
                target = executePatientQuery(view, model);
            }

            /**
             * If the query was successful, store the result in the database.
             */
            if(target != null) {
                Marshaller marshaller = context.createMarshaller();
                StringWriter writer = new StringWriter();
                ObjectFactory factory = new ObjectFactory();

                if(target instanceof QueryResult) {
                    marshaller.marshal(factory.createQueryResult((QueryResult) target), writer);
                } else if(target instanceof Error) {
                    marshaller.marshal(target, writer);
                }

                request.setStatus(RequestStatus.EXECUTED);
                request.setResult(writer.toString());

                dao.updateRequest(request);

                store.commit();
            }
        } catch(Exception e) {
            e.printStackTrace();

            try (StoreConnection store = ConnectionFactory.get()) {
                model = ConnectionFactory.getModel();
                model.injectLogin("export");

                RequestDAO dao = store.get(RequestDAO.class);
                Request request = dao.getRequest(queryId);

                JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);
                Marshaller marshaller = context.createMarshaller();
                StringWriter writer = new StringWriter();

                Error error = new Error();
                error.setErrorCode(500);
                error.setDescription("Server error!");

                marshaller.marshal(error, writer);

                request.setStatus(RequestStatus.EXECUTED);
                request.setResult(writer.toString());

                dao.updateRequest(request);

                store.commit();
            } catch (DatabaseException | JAXBException | DAOException e1) {
            }
        } finally {
            try {
                model.close();
            } catch (DatabaseException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Executes a patient query. Currently only one type of query is supported:
     * the full export.
     *
     * @param view the query view
     * @param model the PSQLModel which will be used to execute the query
     * @return
     * @throws DatabaseException
     */
    private Object executePatientQuery(View view, PSQLModel<Void> model) throws DatabaseException {
        logger.debug("Executing patient query");

        // Check if the view is a full export
        List<String> keys = Collections.emptyList();

        if(view.getViewFields() != null) {
            keys = view.getViewFields().getMdrKey();
        }

        if(keys.size() == 1 && keys.get(0).equals("*")) {
            if(where.getAndOrEqOrLike().size() != 0) {
                Error error = new Error();
                error.setDescription("Full export but with an Query!");
                return error;
            } else {
                QueryResult result = fullExport(view, model);
                return result;
            }
        } else {
            try {
                return getPatientQuery(view, model);
            } catch (UnknownAttributesException e) {
                de.samply.share.model.osse.Error error = new Error();
                error.getMdrKey().addAll(e.getAttributes());
                error.setDescription("Unknown attributes");
                error.setErrorCode(400);
                return error;
            } catch(ParseException e) {
                de.samply.share.model.osse.Error error = new Error();
                return error;
            }
        }
    }

    /**
     * Executes the view as a normal Patient Query
     * @param view
     * @param model
     * @return
     * @throws DatabaseException
     * @throws UnknownAttributesException
     * @throws ParseException
     */
    private Object getPatientQuery(View view, PSQLModel<Void> model) throws DatabaseException, UnknownAttributesException, ParseException {
        QueryConfig qConfig = new QueryConfig();

        ResourceQueryBuilder builder = new ResourceQueryBuilder(view, model.getConfig(OSSEOntology.Configs.OSSE), qConfig);
        QueryUtil util = new QueryUtil();

        return util.getResult(builder.execute(model));
    }

    /**
     * @param view
     * @return
     * @throws DatabaseException
     */
    private QueryResult fullExport(View view, PSQLModel<Void> model) throws DatabaseException {
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        HashMap<String, Date> where = new HashMap<>();
        logger.debug("Doing a full export.");

        /**
         * Check the where part.
         */
        try {
            if(view.getQuery() != null && view.getQuery().getWhere() != null && view.getQuery().getWhere().getAndOrEqOrLike().size() > 0) {
                And and = (And) view.getQuery().getWhere().getAndOrEqOrLike().get(0);

                for(Object obj : and.getAndOrEqOrLike()) {
                    if(obj instanceof Gt) {
                        Gt gt = (Gt) obj;
                        String value = gt.getAttribute().getValue().getValue();
                        where.put(gt.getAttribute().getMdrKey() + "_gt", simpleDateFormat.parse(value));
                    }

                    if(obj instanceof Leq) {
                        Leq leq = (Leq) obj;
                        String value = leq.getAttribute().getValue().getValue();
                        where.put(leq.getAttribute().getMdrKey() + "_leq", simpleDateFormat.parse(value));
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Boolean filter = where.keySet().size() > 0;

        QueryUtil util = new QueryUtil();

        QueryResult result = util.getResult(model.getResources(OSSEVocabulary.Type.Patient), model, where, filter);

        result.setId("" + queryId);

        return result;
    }

}
