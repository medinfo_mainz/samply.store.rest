/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest;

import java.io.FileNotFoundException;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.xml.sax.SAXException;

import de.samply.config.util.FileFinderUtil;
import de.samply.store.osse.OSSEModel;
import de.samply.store.rest.dao.ConnectionFactory;
import de.samply.store.rest.dao.DAOException;
import de.samply.store.rest.dao.RequestDAO;
import de.samply.store.rest.dao.StoreConnection;

/**
 * Initializes the config files, the database if necessary, the
 * OSSEModel configuration, and starts the cleanup task.
 *
 */
@WebListener
public class ServletListener implements ServletContextListener {

    private static Logger logger = LogManager.getLogger(ServletListener.class);

    private Timer timer;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            Configurator.initialize(null, (ClassLoader) null, FileFinderUtil.findFile("log4j2.xml", "osse",
                    sce.getServletContext().getRealPath("/WEB-INF")).toURI());

            /**
             * Load the PostgreSQL driver
             */
            Class.forName("org.postgresql.Driver").newInstance();

            /**
             * load all configuration files (except the above log4j2.xml
             */
            RestConfig.initialize(sce.getServletContext());

            /**
             * If necessary, create the database tables
             */
            try(StoreConnection connection = ConnectionFactory.get()) {
                connection.initialize();
                connection.commit();

                logger.debug("Currently " + connection.get(RequestDAO.class).getRequests().size() + " open queries");
            }

            /**
             * Let OSSEModel load the resources.xml
             */
            OSSEModel.init();

            /**
             * Start the cleanup task
             */
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    try(StoreConnection store = ConnectionFactory.get()) {
                        logger.debug("Cleaning up old REST requests");
                        store.get(RequestDAO.class).cleanup();
                        store.commit();
                    } catch (DAOException e) {
                        e.printStackTrace();
                    }
                }
            };

            timer = new Timer();
            timer.scheduleAtFixedRate(task, 5000, 1000 * 60 * 60);

        } catch(FileNotFoundException | InstantiationException | IllegalAccessException
                | ClassNotFoundException | JAXBException | SAXException | ParserConfigurationException | DAOException
                | SQLException e) {
            logger.debug("Error initializing servlet: ", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        /**
         * Unload the drivers.
         */
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
                logger.info("Unregistering driver " + driver.toString());
            } catch (SQLException e) {
            }
        }

        if(timer != null) {
            logger.debug("Stopping cleanup timer");
            timer.cancel();
        }

        logger.info("Context destroyed.");
    }

}
