/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.sql.Date;
import java.util.Calendar;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.samply.auth.client.AuthClient;
import de.samply.auth.client.InvalidKeyException;
import de.samply.auth.client.InvalidTokenException;
import de.samply.auth.client.jwt.KeyLoader;
import de.samply.auth.rest.AccessTokenDTO;
import de.samply.share.model.osse.QueryResult;
import de.samply.share.model.osse.QueryResultStatistic;
import de.samply.share.model.osse.View;
import de.samply.store.JSONResource;
import de.samply.store.PSQLModel;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.rest.dao.ConnectionFactory;
import de.samply.store.rest.dao.DAOException;
import de.samply.store.rest.dao.Request;
import de.samply.store.rest.dao.RequestDAO;
import de.samply.store.rest.dao.RequestStatus;
import de.samply.store.rest.dao.StoreConnection;

/**
 * The REST resources that give access to:
 *
 * <pre>
 * - /requests
 * - /requests/{id}/result
 * - /requests/{id}/stats
 * </pre>
 *
 *
 */
@Path("/")
public class StoreRest {

    private static Logger logger = LogManager.getLogger(StoreRest.class);

    /**
     * Defines the number of enities on one page.
     */
    private static final int maxEntitiesPerPage = 10;

    /**
     * Accepts a view and starts a thread to execute this query. Stores the request in the database.
     * @param view
     * @return
     * @throws DatabaseException
     * @throws JAXBException
     */
    @POST
    @Path("/requests")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.APPLICATION_XML)
    public Response createQuery(View view) throws DatabaseException, JAXBException {
        try(StoreConnection store = ConnectionFactory.get()) {
            logger.debug("Received a query, storing it and starting QueryThread");

            RequestDAO dao = store.get(RequestDAO.class);

            Request request = new Request();

            JAXBContext context = JAXBContext.newInstance(View.class);

            StringWriter writer = new StringWriter();
            context.createMarshaller().marshal(view, writer);

            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_YEAR, 10);

            request.setQuery(writer.toString());
            request.setResult("");
            request.setExpirationDate(new Date(cal.getTime().getTime()));
            request.setStatus(RequestStatus.REQUESTED);

            logger.debug("Storing request");
            dao.saveRequest(request);
            logger.debug("Request stored with id " + request.getId());

            store.commit();

            QueryThread thread = new QueryThread(request.getId());
            thread.start();

            return Response.created(new URI("requests/" + request.getId()))
                    .header(HttpHeaders.EXPIRES, cal.getTime()).build();
        } catch (DAOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        throw new BadRequestException();
    }

    /**
     * Returns a valid access token from samply auth. This still needs to use the correct proxy settings and stuff.
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/access_token")
    public AccessTokenDTO getAccessToken() {
        PSQLModel<Void> model = null;

        try(StoreConnection store = ConnectionFactory.get()) {
            model = ConnectionFactory.getModel();
            model.injectLogin("export");

            JSONResource osseConfig = model.getConfig("osse");
            String key = osseConfig.getProperty("auth.my.privkey").getValue();
            String authUrl = osseConfig.getProperty("auth.rest").getValue();
            String publicKey = osseConfig.getProperty("auth.server.pubkey").getValue();

            PrivateKey privateKey = KeyLoader.loadPrivateKey(key);
            PublicKey pubKey = KeyLoader.loadKey(publicKey);

            // TODO: Load proxy settings.
            AuthClient client = new AuthClient(authUrl, pubKey, privateKey, ClientBuilder.newBuilder().build());
            client.getAccessToken();

            AccessTokenDTO dto = new AccessTokenDTO();
            dto.setAccessToken(client.getAccessToken().getSerialized());

            if(client.getIDToken() != null) {
                dto.setIdToken(client.getIDToken().getSerialized());
            }

            return dto;
        } catch(InvalidTokenException | InvalidKeyException k) {
            throw new BadRequestException();
        } catch (DAOException | DatabaseException e) {
            throw new ServerErrorException(Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Returns the results for the specified request. Maximum 50 entities per QueryResult
     * @param strQueryId
     * @param strPage
     * @return
     * @throws DAOException
     * @throws JAXBException
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Path("/requests/{id}/result")
    public Response getResultOnPage(@PathParam("id") String strQueryId, @QueryParam("page") String strPage) throws DAOException,
                JAXBException {
        logger.debug("Requesting query " + strQueryId + ", page " + strPage);

        QueryResult result = new QueryResult();

        if(strPage == null) {
            strPage = "0";
        }

        int queryId, page;

        try {
            queryId = Integer.parseInt(strQueryId);
            page = Integer.parseInt(strPage);
        } catch(NumberFormatException e) {
            throw new BadRequestException();
        }

        try(StoreConnection store = ConnectionFactory.get()) {
            Request request = store.get(RequestDAO.class).getRequest(queryId);

            if(request == null) {
                logger.debug("Query " + strQueryId + " not found.");
                return Response.status(404).build();
            }

            if(request.getStatus() != RequestStatus.EXECUTED) {
                logger.debug("Query is still being executed");
                return Response.accepted().build();
            }

            JAXBContext context = JAXBContext.newInstance(QueryResult.class);

            Object object = context.createUnmarshaller().unmarshal(new StringReader(request.getResult()));

            if(object instanceof QueryResult) {
                QueryResult fullResult = (QueryResult) object;

                for(int i = page * maxEntitiesPerPage; i < (page + 1) * maxEntitiesPerPage && i < fullResult.getEntity().size(); ++i) {
                    logger.debug("Adding entity " + i);
                    result.getEntity().add(fullResult.getEntity().get(i));
                }
            } else {
                return Response.ok(object).build();
            }

        }

        return Response.ok(result).build();
    }

    /**
     * Returns the statistics: the number of pages, the number of entities and the id.
     * @param strQueryId the query ID
     * @return the QueryResultStatistic for the given query ID
     * @throws DAOException
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Path("/requests/{id}/stats")
    public Response getStatistics(@PathParam("id") String strQueryId) throws DAOException {
        logger.debug("Requesting query " + strQueryId);

        int queryId;

        try {
            queryId = Integer.parseInt(strQueryId);
        } catch(NumberFormatException e) {
            throw new BadRequestException();
        }

        try(StoreConnection connection = ConnectionFactory.get()) {
            Request request = connection.get(RequestDAO.class).getRequest(queryId);

            if(request == null) {
                logger.debug("Query " + strQueryId + " not found.");
                return Response.status(404).build();
            }

            if(request.getStatus() != RequestStatus.EXECUTED) {
                logger.debug("Query is still being executed");
                return Response.accepted().build();
            }

            JAXBContext context = JAXBContext.newInstance(QueryResult.class, QueryResultStatistic.class);

            Object instance = context.createUnmarshaller().unmarshal(new StringReader(request.getResult()));

            if(instance instanceof JAXBElement<?>) {
                QueryResult result = (QueryResult)((JAXBElement<?>) instance).getValue();

                logger.debug("Query was executed, entities: " + result.getEntity().size());

                QueryResultStatistic stats = new QueryResultStatistic();
                stats.setNumberOfPages(result.getEntity().size() / maxEntitiesPerPage +
                        (result.getEntity().size() % maxEntitiesPerPage == 0 ? 0 : 1));
                stats.setTotalSize(result.getEntity().size());
                stats.setRequestId("" + request.getId());
                return Response.ok(stats).build();
            } else {
                return Response.status(422).entity(instance).build();
            }

        } catch (JAXBException e) {
            logger.error("JAXB Exception", e);
        }

        throw new BadRequestException();
    }

}
