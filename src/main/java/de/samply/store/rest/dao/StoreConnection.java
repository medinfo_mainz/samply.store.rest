/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A connection to the database.
 *
 */
public class StoreConnection extends ConnectionProvider {

    private static final Logger logger = LogManager.getLogger(StoreConnection.class);

    /**
     * Initializes a new connection to the database.
     * @param host
     * @param database
     * @param user
     * @param password
     */
    public StoreConnection(String host, String database, String user, String password) {
        super(host, database, user, password);
    }

    /**
     * Initializes the database. It creates the necessary SQL types and tables if needed.
     * @throws SQLException
     */
    public void initialize() throws SQLException {
        Connection connection = get();

        PreparedStatement stmt = connection.prepareStatement("select exists (select 1 from pg_type where typname = 'reqStatus');");
        stmt.execute();
        ResultSet resultSet = stmt.getResultSet();

        if(resultSet.next() && !resultSet.getBoolean(1)) {
            logger.debug("Creating type reqStatus");
            stmt = connection.prepareStatement("CREATE TYPE \"reqStatus\" AS ENUM ('REQUESTED', 'EXECUTING', 'EXECUTED')");
            stmt.execute();
            stmt.close();
        }
        stmt.close();

        logger.debug("Creating table if it does not exist");
        stmt = connection.prepareStatement("CREATE TABLE IF NOT EXISTS \"osse_rest_request\" (id SERIAL PRIMARY KEY, "
                + "query TEXT NOT NULL, result TEXT NOT NULL, \"expiration\" DATE NOT NULL, "
                + "status \"reqStatus\" NOT NULL)");

        stmt.execute();
        stmt.close();
    }

    @Override
    public int getRequiredVersion() throws DAOException {
        return 0;
    }
}
