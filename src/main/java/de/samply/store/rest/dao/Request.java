/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest.dao;

import java.io.Serializable;
import java.sql.Date;

/**
 * A request to execute a query.
 *
 */
public class Request implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The actual query as XML.
     */
    private String query;

    /**
     * The result as XML.
     */
    private String result;

    /**
     * The time when the request was made.
     */
    private Date expirationDate;

    /**
     * The requests id.
     */
    private int id;

    /**
     * The current request status (REQUESTED, EXECUTING, EXECUTED).
     */
    private RequestStatus status;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date time) {
        this.expirationDate = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

}
