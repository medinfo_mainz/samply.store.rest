/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * The DAO used to access the request table.
 *
 */
public class RequestDAO extends AbstractDAO<Request> {

    protected RequestDAO(ConnectionProvider provider) {
        super(provider);
    }

    /**
     * Returns all requests that are currently in the REQUESTED status.
     * @return
     * @throws DAOException
     */
    public List<Request> getRequests() throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE req.status = 'REQUESTED'");
    }

    /**
     * Saves the given request in the database.
     * @param request
     * @throws DAOException
     */
    public void saveRequest(Request request) throws DAOException {
        request.setId(insert(request.getQuery(), request.getResult(), request.getExpirationDate(), request.getStatus()));
    }

    /**
     * Returns the request by its ID.
     * @param id
     * @return
     * @throws DAOException
     */
    public Request getRequest(int id) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE req.id = ?", id);
    }

    /**
     * Updated the given request. Only possible for requests that are currently "REQUESTED".
     * @param request
     * @throws DAOException
     */
    public void updateRequest(Request request) throws DAOException {
        executeUpdate("UPDATE osse_rest_request SET result = ?, status = ?::\"reqStatus\" WHERE id = ? AND status = 'REQUESTED'", request.getResult(),
                request.getStatus(), request.getId());
    }

    /**
     * Deletes the old requests that are no longer valid.
     * @return
     * @throws DAOException
     */
    public int cleanup() throws DAOException {
        return executeUpdate("DELETE FROM osse_rest_request WHERE expiration < now()");
    }

    @Override
    public Request getObject(ResultSet set) throws SQLException {
        Request request = new Request();
        request.setId(set.getInt("req_id"));
        request.setQuery(set.getString("req_query"));
        request.setResult(set.getString("req_result"));
        request.setExpirationDate(set.getDate("req_expiration"));
        request.setStatus(RequestStatus.valueOf(set.getString("req_status")));
        return request;
    }

    @Override
    protected String getInsertTable() {
        return "osse_rest_request";
    }

    @Override
    protected String getInsertFields() {
        return "query, result, expiration, status";
    }

    public static final String SQL_TABLE = "osse_rest_request AS req";

    public static final String SQL_SELECT_FIELDS = "req.\"id\" AS \"req_id\", req.\"expiration\" AS \"req_expiration\", "
            + "req.\"query\" AS \"req_query\", req.\"result\" AS \"req_result\", req.\"status\" AS \"req_status\"";

}
