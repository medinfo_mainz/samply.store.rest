/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest.dao;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;

import de.samply.store.JSONResource;
import de.samply.store.Value;

/**
 * A basic connection provider that provides a SQL connection, implements
 * AutoCloseable (should be used in try blocks) and a way to create
 * new DAOs.
 *
 *
 */
public abstract class ConnectionProvider extends SQLConnection implements AutoCloseable {

    /**
     * The current database credentials
     */
    protected final String host, database, username, password;

    /**
     * The map that stores all DAOs
     */
    protected HashMap<Class<?>, Object> map = new HashMap<>();

    /**
     * The SQL connection.
     */
    protected Connection connection;

    /**
     * Initializes this connection provider with the given database credentials.
     *
     * @param host
     * @param database
     * @param username
     * @param password
     */
    protected ConnectionProvider(String host, String database, String username, String password) {
        super(null);
        this.host = host;
        this.database = database;
        this.username = username;
        this.password = password;
    }

    /**
     * Closes the underlaying SQL connection. Executes a rollback. So before you close the connection,
     * make sure you commit your data.
     */
    @Override
    public final void close() throws DAOException {
        map.clear();
        try {
            if(connection != null) {
                connection.rollback();
                connection.close();
            }
        } catch(SQLException e) {
            throw new DAOException("Exception in Close!", e);
        }
    }

    /**
     * Executes a rollback on the SQL connection if there is one.
     * @throws DAOException
     */
    public final void rollback() throws DAOException {
        try {
            if(connection != null) {
                connection.rollback();
            }
        } catch (SQLException e) {
            throw new DAOException("Exception in Rollback!", e);
        }
    }

    /**
     * Returns the SQL connection. Establishes one, if there is currently none.
     * Disables the auto commit feature.
     * @return
     */
    @Override
    public final Connection get() throws SQLException {
        if(connection == null) {
            String url = "jdbc:postgresql://" + host + "/" +
                    database +
                    "?user=" + username +
                    "&password=" + password;

            connection = DriverManager.getConnection(url);
            connection.setAutoCommit(false);
        }
        return connection;
    }

    /**
     * Executes a commit on the SQL connection if there is one.
     * @throws DAOException
     */
    public final void commit() throws DAOException {
        try {
            if(connection != null) {
                connection.commit();
            }
        } catch (SQLException e) {
            throw new DAOException("Exception in Commit!", e);
        }
    }

    /**
     * Returns an instance of the requested class. The requested class should be an
     * instance of AbstractDAO.
     *
     * @param clazz
     * @return
     */
    @SuppressWarnings("unchecked")
    public final <T extends AbstractDAO<?>> T get(Class<T> clazz) {
        try {
            if(!map.containsKey(clazz)) {
                Constructor<T> constructor = clazz.getDeclaredConstructor(ConnectionProvider.class);
                constructor.setAccessible(true);
                map.put(clazz, constructor.newInstance(this));
            }
            return (T) map.get(clazz);
        } catch (NoSuchMethodException | SecurityException | InstantiationException
                | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns the current configuration. Uses the default configuration as template.
     *
     * @return
     * @throws DAOException
     */
    public final JSONResource getConfig() throws DAOException {
        ConfigDAO dao = get(ConfigDAO.class);

        JSONResource def = dao.get("defaultConfig");
        JSONResource current = dao.get("config");

        int dbVersion = def.getProperty("dbVersion").asInteger();

        for(String property : current.getDefinedProperties()) {
            def.removeProperties(property);
            for(Value value : current.getProperties(property)) {
                def.addProperty(property, value);
            }
        }

        def.removeProperties("dbVersion");
        def.setProperty("dbVersion", dbVersion);

        return def;
    }

    /**
     * Saves the specified configuration. Uses the defaultConfiguration
     * as template. Only the differences are saved!
     * @param config
     * @throws DAOException
     */
    public final void saveConfig(JSONResource config) throws DAOException {
        ConfigDAO dao = get(ConfigDAO.class);

        JSONResource def = dao.get("defaultConfig");
        JSONResource update = new JSONResource();

        for(String v : config.getDefinedProperties()) {
            for(Value value : config.getProperties(v)) {
                if(!def.getProperties(v).contains(value) && !("dbVersion".equals(v))) {
                    update.addProperty(v, value);
                }
            }
        }

        dao.updateConfig("config", update);
    }

    /**
     * Returns the current database version;
     * @return
     * @throws DAOException
     */
    public final int getCurrentVersion() throws DAOException {
        return get(ConfigDAO.class).get("defaultConfig").getProperty("dbVersion").asInteger();
    }

    /**
     * Must return the required version for the implementing
     * ConnectionProvider.
     *
     * @return
     * @throws DAOException
     */
    public abstract int getRequiredVersion() throws DAOException;

}
