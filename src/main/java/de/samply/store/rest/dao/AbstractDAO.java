/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * An abstract data access object, that simplifies interacting with the database.
 *
 * @param <T>
 */
public abstract class AbstractDAO<T> extends SQLConnection implements ObjectMapper<T> {

    protected AbstractDAO(ConnectionProvider provider) {
        super(provider);
    }

    /**
     * Execute a select statement and returns an object
     * @param sql
     * @param binder
     * @return
     * @throws DAOException
     */
    protected T executeSingleSelect(String sql, final Object... object) throws DAOException {
        return executeSingleSelect(sql, this, object);
    }

    /**
     * Execute a select statement and returns an object
     * @param sql
     * @param binder
     * @return
     * @throws DAOException
     */
    protected List<T> executeSelect(String sql, final Object... object) throws DAOException {
        return executeSelect(sql, this, object);
    }

    /**
     * Inserts the given values into the table for this DAO.
     * @param object
     * @return
     * @throws DAOException
     */
    protected int insert(final Object... object) throws DAOException {
        return insertRaw(getInsertTable(), getInsertFields(), object);
    }

    /**
     * Creates a new Object and maps the set.
     */
    @Override
    public abstract T getObject(ResultSet set) throws SQLException;

    /**
     * Returns the table for this DAO. Use quotes for upper case letters.
     * @return
     */
    protected abstract String getInsertTable();

    /**
     * Returns a "list" of all fields. This will not be parsed, just used in the insert method.
     * @return
     */
    protected abstract String getInsertFields();

}
