/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import de.samply.store.BooleanLiteral;
import de.samply.store.JSONResource;
import de.samply.store.StringLiteral;
import de.samply.store.TimestampLiteral;
import de.samply.string.util.StringUtil;
import de.samply.string.util.StringUtil.Builder;

/**
 * This class offers various methods to execute SQL statements easily.
 *
 */
public class SQLConnection {

    private static final Logger logger = LogManager.getLogger(SQLConnection.class);

    /**
     * The connection provider.
     */
    protected ConnectionProvider provider;

    /**
     * Initializes this SQL connection.
     * @param provider
     */
    protected SQLConnection(ConnectionProvider provider) {
        this.provider = provider;
    }

    /**
     * Returns the SQL connection. Uses the provider to get one.
     *
     * @return
     * @throws SQLException
     */
    protected Connection get() throws SQLException {
        return provider.get();
    }

    /**
     * Executes an SQL UPDATE statement and returns the count of affected rows.
     *
     * @param sql the correctly quoted SQL string
     * @param objects the parameters for the SQL statement
     * @return
     * @throws DAOException
     */
    protected int executeUpdate(String sql, Object... objects) throws DAOException {
        return executeUpdate(sql, getArrayBinder(objects));
    }

    /**
     * Executes an SQL UPDATE statement and returns the count of affected rows.
     *
     * @param sql the correctly quoted SQL string
     * @param binder the binder
     * @return
     * @throws DAOException
     */
    private int executeUpdate(String sql, SQLBinder binder) throws DAOException {
        logger.debug("Executing " + sql);
        try(PreparedStatement statement = get().prepareStatement(sql)) {
            binder.bind(statement);
            statement.execute();
            return statement.getUpdateCount();
        }  catch(SQLException e) {
            throw new DAOException("Error in UPDATE!", e);
        }
    }

    /**
     * Executes a SELECT statement, maps the result set to objects using the given ObjectMapper and returns
     * those objects in a List.
     *
     * @param sql the correctly quoted SQL string
     * @param mapper the object mapper
     * @param objects the parameters for the SQL statement
     * @return
     * @throws DAOException
     */
    protected <A> List<A> executeSelect(String sql, ObjectMapper<A> mapper, Object... objects) throws DAOException {
        return executeSelect(sql, getArrayBinder(objects), mapper);
    }

    /**
     * Executes a SELECT statement, maps the result set to objects using the given ObjectMapper and returns
     * those objects in a List.
     *
     * @param sql the correctly quoted SQL string
     * @param binder the binder
     * @param mapper the object mapper
     * @return
     * @throws DAOException
     */
    protected <A> List<A> executeSelect(String sql, SQLBinder binder, ObjectMapper<A> mapper) throws DAOException {
        logger.debug("Executing " + sql);
        try(PreparedStatement statement = get().prepareStatement(sql)) {
            binder.bind(statement);

            ResultSet resultSet = statement.executeQuery();
            List<A> target = new ArrayList<>();

            while(resultSet.next()) {
                A obj = mapper.getObject(resultSet);
                if(obj != null) {
                    target.add(obj);
                }
            }

            return target;
        } catch(SQLException e) {
            throw new DAOException("Error in SELECT!", e);
        }
    }

    /**
     * Executes a SELECT statement, maps the result set to objects using the given ObjectMapper.
     * Returns the <b>first</b> object only. If there are no objects in the
     * result set, this method returns null. If there is more than one object, this method throws an exception.
     *
     * @param sql The correctly quoted SQL statement
     * @param mapper the object mapper
     * @param objects the parameters for the SQL statement
     * @return
     * @throws DAOException
     */
    protected <A> A executeSingleSelect(String sql, ObjectMapper<A> mapper, Object... objects) throws DAOException {
        return executeSingleSelect(sql, getArrayBinder(objects), mapper);
    }

    /**
     * Executes a SELECT statement, maps the result set to objects using the given ObjectMapper.
     * Returns the <b>first</b> object only. If there are no objects in the
     * result set, this method returns null. If there is more than one object, this method throws an exception.
     *
     * @param sql The correctly quoted SQL statement
     * @param binder The binder that binds the values to the statement
     * @param mapper the object mapper
     * @return
     * @throws DAOException
     */
    private <A> A executeSingleSelect(String sql, SQLBinder binder, ObjectMapper<A> mapper) throws DAOException {
        List<A> result = executeSelect(sql, binder, mapper);

        if(result.size() == 1) {
            return result.get(0);
        } else if(result.size() > 1) {
            throw new DAOException("More results than expected");
        } else {
            return null;
        }
    }

    /**
     * Execute an insert statement and returns the returned primary key.
     *
     * @param table the table name. Use quotes if necessary.
     * @param fields the comma separated column names. Use quotes if necessary.
     * @param object
     * @return
     * @throws DAOException
     */
    protected int insertRaw(String table, String fields, Object... object) throws DAOException {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO ").append(table);
        builder.append(" (").append(fields);
        builder.append(") VALUES ").append(constructBinds(object));

        String sql = builder.toString();
        logger.debug("Executing " + sql);

        try(PreparedStatement statement = get().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            SQLBinder binder = getArrayBinder(object);
            binder.bind(statement);

            statement.execute();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            if(generatedKeys.next()) {
                return generatedKeys.getInt(1);
            } else {
                throw new DAOException("Insert Failed!");
            }
        } catch(SQLException e) {
            throw new DAOException("ERROR in Insert!", e);
        }
    }

    /**
     * Inserts the default values into the specified table.
     *
     * @param table
     * @return
     * @throws DAOException
     */
    protected int insertDefault(String table) throws DAOException {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO ").append(table);
        builder.append(" DEFAULT VALUES ");

        String sql = builder.toString();
        logger.debug("Executing " + sql);

        try(PreparedStatement statement = get().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

            statement.execute();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            if(generatedKeys.next()) {
                return generatedKeys.getInt(1);
            } else {
                throw new DAOException("Insert Failed!");
            }
        } catch(SQLException e) {
            throw new DAOException("ERROR in Insert!", e);
        }
    }

    /**
     * Returns a SQLBinder for the given array of Objects.
     * @param object
     * @return
     */
    private SQLBinder getArrayBinder(final Object... object) {
        return new SQLBinder() {
            @Override
            public void bind(PreparedStatement statement) throws SQLException {
                int index = 0;
                if(object != null) {
                    for(int i = 0; i < object.length; ++i) {
                        if(object[i] instanceof List<?>) {
                            List<?> list = (List<?>) object[i];
                            for(Object  o : list) {
                                statement.setObject(++index, o);
                            }
                        } else if(object[i] instanceof SQLEnum) {
                            statement.setObject(++index, object[i].toString());
                        } else if(object[i] instanceof JSONResource) {
                            statement.setObject(++index, ((JSONResource) object[i]).toJson());
                        } else {
                            if(!(object[i] instanceof SQLFunction)) {
                                statement.setObject(++index, object[i]);
                            }
                        }
                    }
                }
            }
        };
    }

    /**
     * Constructs a string of "values" for an array of objects.
     *
     * @param object
     * @return
     */
    private String constructBinds(Object... object) {
        return "(" + StringUtil.join(object, ", ", new Builder<Object>() {
            @Override
            public String build(Object o) {
                return construct(o);
            }
        }) + ")";
    }

    /**
     * Returns the corresponding "SQL-Value" for the specified object.
     *
     * ?::"SQL-ENUM" for enums
     * CAST(? AS JSON) for JSONResource
     * ? for everything else
     *
     * @param o
     * @return
     */
    private String construct(Object o) {
        if(o instanceof SQLFunction) {
            return ((SQLFunction) o).getMethod();
        } if(o instanceof SQLEnum) {
            SQLEnum e = (SQLEnum) o;
            return "?::\"" + e.getSQLName() + "\"";
        } if(o instanceof JSONResource) {
            return "CAST(? AS JSON)";
        } else {
            return "?";
        }
    }

    /**
     * Returns a JSONResource using the specified JSON string.
     * @param input
     * @return
     */
    protected JSONResource asJson(String input) {
        Gson gson = new Gson();
        JSONResource resource = new JSONResource();
        addValues(resource, gson.fromJson(input, JsonObject.class));
        return resource;
    }

    /**
     * Converts the specified JsonObject into a JSONResource
     * @param resource
     * @param obj
     */
    private void addValues(JSONResource resource, JsonObject obj) {
        for(Entry<String, JsonElement> entry : obj.entrySet()) {
            addValues(resource, entry.getKey(), entry.getValue());
        }
    }

    /**
     * Add the specified JsonElement to the specified JSONResource. Works recursively.
     * @param resource
     * @param property
     * @param element
     */
    private void addValues(JSONResource resource, String property, JsonElement element) {
        if(element.isJsonPrimitive()) {
            if(element.getAsJsonPrimitive().isBoolean()) {
                resource.addProperty(property, new BooleanLiteral(element.getAsBoolean()));
            } else if(element.getAsJsonPrimitive().isNumber()) {
                resource.addProperty(property, element.getAsNumber());
            } else {
                String str = element.getAsJsonPrimitive().getAsString();
                try {
                    resource.addProperty(property, new TimestampLiteral(Timestamp.valueOf(str)));
                } catch(IllegalArgumentException e) {
                    resource.addProperty(property, new StringLiteral(str));
                }
            }
        }

        if(element.isJsonArray()) {
            for(JsonElement e : element.getAsJsonArray()) {
                addValues(resource, property, e);
            }
        }

        if(element.isJsonObject()) {
            JSONResource res = new JSONResource();
            addValues(res, element.getAsJsonObject());
            resource.addProperty(property, res);
        }
    }

    /***
     *
     * Just some methods to execute *RAW* SQL
     *
     */

    /**
     * Reads all strings from the reader and executes them as SQL statements.
     * Uses ';' as separator. This method does <b>not</b> work for statements
     * that define new functions in SQL.
     *
     * @param reader
     */
    public void executeStream(Reader reader) {
        String s = new String();
        StringBuffer sb = new StringBuffer();

        BufferedReader br = new BufferedReader(reader);

        try {
            while ((s = br.readLine()) != null) {
                sb.append(s);
            }
            br.close();

            // here is our splitter ! We use ";" as a delimiter for each request
            // then we are sure to have well formed statements
            executeSQL(sb.toString());

        } catch (Exception e) {
            logger.error("*** Error : " + e.toString());
            logger.error("*** ");
            logger.error("*** Error : ", e);
            logger.error("################################################");
        }
    }

    /**
     * Executes all SQL statements in the given String. This method does
     * <b>not</b> work for statements that define new functions in SQL.
     *
     * @param sql
     * @throws SQLException
     */
    public void executeSQL(String sql) throws SQLException {
        String[] inst = sql.split(";");

        Statement st = get().createStatement();

        for (int i = 0; i < inst.length; i++) {
            // we ensure that there is no spaces before or after the request
            // string in order to not execute empty statements
            if (!inst[i].trim().equals("")) {
                st.addBatch(inst[i]);
                logger.debug("executing " + inst[i]);
            }
        }
        st.executeBatch();
        st.close();
    }

    /**
     * Parses the given file as SQL file and executes all statements.
     *
     * @param filename file path
     */
    public void executeFile(String filename) {
        try {
            FileReader fr = new FileReader(new File(filename));

            executeStream(fr);

            fr.close();
        } catch (Exception e) {
            logger.error("*** Error : " + e.toString());
            logger.error("*** ");
            logger.error("*** Error : ", e);
            logger.error("################################################");
        }
    }

}
