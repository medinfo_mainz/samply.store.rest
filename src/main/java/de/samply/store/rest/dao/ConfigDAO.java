/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import de.samply.store.JSONResource;

/**
 * The DAO to get and save configuration properties.
 *
 */
public class ConfigDAO extends AbstractDAO<JSONResource> {

    /**
     * Initializes this DAO.
     *
     * @param connection
     */
    protected ConfigDAO(ConnectionProvider connection) {
        super(connection);
    }

    /**
     * Returns the configuration object with the given name. Null if the requested configuration
     * object does not exist.
     *
     * @param name configuration name
     * @return The requested configuration object, null if it does not exist.
     * @throws DAOException
     */
    public JSONResource get(String name) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE c.name = ?", name);
    }

    @Override
    public JSONResource getObject(ResultSet set) throws SQLException {
        return asJson(set.getString("c_value"));
    }

    /**
     * Updates the configuration object with the given name.
     *
     * @param name configuration name
     * @param config the new configuration object
     * @return number of affected rows
     * @throws DAOException
     */
    public int updateConfig(String name, JSONResource config) throws DAOException {
        return executeUpdate("UPDATE config SET value = CAST(? AS JSON) WHERE name = ?", config, name);
    }

    @Override
    protected String getInsertTable() {
        return "config";
    }

    @Override
    protected String getInsertFields() {
        return "name, value";
    }

    public static final String SQL_TABLE = "config AS c";

    public static final String SQL_SELECT_FIELDS = "c.name AS \"c_name\", c.value AS \"c_value\"";

}
