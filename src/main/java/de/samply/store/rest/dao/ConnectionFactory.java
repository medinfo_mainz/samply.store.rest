/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest.dao;

import de.samply.common.config.Postgresql;
import de.samply.store.AccessController;
import de.samply.store.PSQLModel;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.rest.RestConfig;

/**
 * Creates a StoreConnection or a PSQLModel using the configuration.
 *
 */
public class ConnectionFactory {

    /**
     * Creates a new StoreConnection instance.
     * @return
     */
    public static StoreConnection get() {
        Postgresql psql = RestConfig.getPostgresql();
        return new StoreConnection(psql.getHost(), psql.getDatabase(), psql.getUsername(), psql.getPassword());
    }

    /**
     * Creates a new PSQLModel instance.
     * @return
     * @throws DatabaseException
     */
    public static PSQLModel<Void> getModel() throws DatabaseException {
        Postgresql psql = RestConfig.getPostgresql();
        return new PSQLModel<Void>(new AccessController<Void>(), psql.getHost(), psql.getDatabase(), psql.getUsername(), psql.getPassword());
    }

}
