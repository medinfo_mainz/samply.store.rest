/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.store.rest;

import java.io.FileNotFoundException;

import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import de.samply.common.config.ObjectFactory;
import de.samply.common.config.Postgresql;
import de.samply.common.config.Store;
import de.samply.config.util.FileFinderUtil;
import de.samply.config.util.JAXBUtil;
import de.samply.store.rest.dao.DAOException;

/**
 * The static configuration for this application.
 *
 */
public class RestConfig {

    private static Postgresql postgresql;

    private static JAXBContext context;

    /**
     * Generates the static JAXBContext.
     * @return
     * @throws JAXBException
     */
    private static JAXBContext getJAXBContext() throws JAXBException {
        if(context == null) {
            context = JAXBContext.newInstance(ObjectFactory.class);
        }
        return context;
    }

    /**
     * Initializes the configuration
     *
     * @param context the servlet context
     * @throws FileNotFoundException
     * @throws JAXBException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws DAOException
     */
    public static void initialize(ServletContext context) throws FileNotFoundException, JAXBException,
            SAXException, ParserConfigurationException, DAOException {
        Store store = JAXBUtil.unmarshall(FileFinderUtil.findFile("backend.xml", "osse", context.getRealPath("/WEB-INF")),
                    getJAXBContext(), Store.class);
        postgresql = store.getPostgresql();
    }

    public static Postgresql getPostgresql() {
        return postgresql;
    }

}
