# Changelog Samply Store OSSE

## Version 1.4.0

- added the CHANGELOG file
- added the `access_token` REST resource
- added support for the new `ResourceQueryBuilder`
- fixed the error handling when executing queries
