--
-- Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
-- Contact: info@osse-register.de
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see <http://www.gnu.org/licenses>.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--



INSERT INTO registries (transaction_id, data) VALUES (1, '{

    "urn:osse-ror:dataelement:1:1":"Register für Nephronophtyse",
    "urn:osse-ror:dataelement:2:1":"NephReg",
    "urn:osse-ror:dataelement:3:1":"Patienten mit Nephronophtyse",
    "urn:osse-ror:dataelement:4:1":true,
    "urn:osse-ror:dataelement:5:1":false,
    "urn:osse-ror:dataelement:6:1":true,
    "urn:osse-ror:dataelement:7:1":true,
    "urn:osse-ror:dataelement:8:1":false,
    "urn:osse-ror:dataelement:9:1":false,
    "urn:osse-ror:dataelement:10:1":true,
    "urn:osse-ror:dataelement:11:1":"Die Lebenserwartung der Patienten soll erheblich gesteigert werden",
    "urn:osse-ror:dataelement:12:1":"Das Register von dem Leiter des Instituts verwaltet und geleitet",
    "urn:osse-ror:dataelement:13:1":"https://nephreg.de",
    "urn:osse-ror:dataelement:14:1":"Prof. Dr. Schulz",
    "urn:osse-ror:dataelement:54:1":true,

    "urn:osse-ror:dataelement:15:1":"Jeder der Verwandte mit Nephronophtyse hat",
    "urn:osse-ror:dataelement:16:1":"regional",
    "urn:osse-ror:dataelement:17:1":"Keine besonderen",
    "urn:osse-ror:dataelement:18:1":false,
    "urn:osse-ror:dataelement:19:1":"2014-12-18",
    "urn:osse-ror:dataelement:20:1":"2015-12-18",
    "urn:osse-ror:dataelement:21:1":1235,
    "urn:osse-ror:dataelement:22:1":"2014-12-18",

    "urn:osse-ror:dataelement:23:1":123,
    "urn:osse-ror:dataelement:24:1":"https://nephreg.org",
    "urn:osse-ror:dataelement:25:1":true,
    "urn:osse-ror:dataelement:26:1":false,
    "urn:osse-ror:dataelement:27:1":true,
    "urn:osse-ror:dataelement:28:1":false,
    "urn:osse-ror:dataelement:29:1":true,
    "urn:osse-ror:dataelement:30:1":true,
    "urn:osse-ror:dataelement:52:1":"central",

    "urn:osse-ror:dataelement:31:1":"https://nephreg.org/datenschutz.html",
    "urn:osse-ror:dataelement:32:1":"Keine Komission vorhanden",

    "urn:osse-ror:dataelement:33:1":"Keine Bedingungen vorhanden",
    "urn:osse-ror:dataelement:34:1":false,
    "urn:osse-ror:dataelement:35:1":true,

    "urn:osse-ror:dataelement:36:1":"Universitätsmedizin Mainz",
    "urn:osse-ror:dataelement:37:2":"JU",
    "urn:osse-ror:dataelement:39:1":"IMBEI",
    "urn:osse-ror:dataelement:40:1":"Obere Zahlbacher Straße 123",
    "urn:osse-ror:dataelement:41:1":"55131",
    "urn:osse-ror:dataelement:42:1":"Mainz",
    "urn:osse-ror:dataelement:43:1":"Germany",
    "urn:osse-ror:dataelement:44:1":"Folz",
    "urn:osse-ror:dataelement:45:1":"Michael",
    "urn:osse-ror:dataelement:46:1":"Mr",
    "urn:osse-ror:dataelement:47:1":"",
    "urn:osse-ror:dataelement:48:1":"Leiter",
    "urn:osse-ror:dataelement:49:1":"Telefon, Post, Internet, reddit, 4chan, IRC on freenode",

    "urn:osse-ror:dataelement:50:1":"Biobank Mainz",
    "urn:osse-ror:dataelement:51:1":"BMZ",
    "urn:osse-ror:dataelement:53:1":"Biomaterialbank dieser komischen Menschen aus dem Süden"

        }');
